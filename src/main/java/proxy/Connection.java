package proxy;

public class Connection implements IConnection {
    private String login;
    private String password;

    public Connection(String login, String password) {
        this.login = login;
        this.password = password;
        saveToCash();
    }

    public Connection() {
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void connect() {
        //checking all user data
        System.out.println(login + "...Connection...Success");
    }

    public void saveToCash(){
        //saving
        System.out.println("Data saved to cash");
    }
}
