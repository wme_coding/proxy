package proxy;

public interface IConnection {
    void connect();

    void setLogin(String login);

    void setPassword(String password);
}
