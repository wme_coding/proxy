package proxy;

public class CashConnection implements IConnection {
    private Connection connection;
    private String login;
    private String password;

    public CashConnection(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public CashConnection() {
    }

    @Override
    public void connect() {
        if(connection == null){
            connection = new Connection(login, password);
        } else {
            System.out.println("Entering with cash");
            return;
        }
        //Checking if user has data cash
        connection.connect();
    }

    @Override
    public void setLogin(String login) {
        this.login = login;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }
}
