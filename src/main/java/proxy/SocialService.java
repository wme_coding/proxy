package proxy;

public class SocialService {
    IConnection connection;
    //other stuff


    public SocialService(IConnection connection) {
        this.connection = connection;
    }

    public void login(String login, String password){
        connection.setLogin(login);
        connection.setPassword(password);
        connection.connect();
    }

    public void logout(){
        //logging out
    }
}
