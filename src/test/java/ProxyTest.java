import org.junit.Test;
import proxy.CashConnection;
import proxy.SocialService;

public class ProxyTest {

    @Test
    public void testProxy(){
        SocialService socialService = new SocialService(new CashConnection());
        socialService.login("qwer", "1234");
        socialService.logout();
        System.out.println();
        socialService.login("qwer", "1234");
    }
}
